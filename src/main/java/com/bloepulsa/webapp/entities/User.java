package com.bloepulsa.webapp.entities;

public class User {
	private long id;
	private String username;
	private String password;
	private long last_login_at;
	
	
	
	
	public User(long id, String username, String password, long last_login_at) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.last_login_at = last_login_at;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getLast_login_at() {
		return last_login_at;
	}
	public void setLast_login_at(long last_login_at) {
		this.last_login_at = last_login_at;
	}
	
	
}
