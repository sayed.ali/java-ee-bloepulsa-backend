package com.bloepulsa.webapp.dto;

import java.io.Serializable;

public class BaseResponseDto<T> implements Serializable {

	private static final long serialVersionUID = 1L;
		
	private int code;
	private String message;
	private T data;
	
	
	
	public BaseResponseDto(int code, String message) {
		this.code = code;
		this.message = message;
	}



	public int getCode() {
		return code;
	}



	public void setCode(int code) {
		this.code = code;
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}



	public T getData() {
		return data;
	}



	public void setData(T data) {
		this.data = data;
	}
	
}
