package com.bloepulsa.webapp.dto;

import java.io.Serializable;

public class LoginResponseDto implements Serializable {

	private static final long serialVersionUID = 1830972522837976630L;
	private String token;
	

	public LoginResponseDto(String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
