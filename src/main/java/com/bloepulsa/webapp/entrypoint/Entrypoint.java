package com.bloepulsa.webapp.entrypoint;

import java.util.HashSet;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.bloepulsa.webapp.handlers.AuthHandler;

@ApplicationPath("/api")
public class Entrypoint extends Application{
	
	private Set<Class<?>> handlers = new HashSet<>();

	@Override
	public Set<Class<?>> getClasses() {

		this.handlers.add(AuthHandler.class);
		
		return this.handlers;
	}
	
}
