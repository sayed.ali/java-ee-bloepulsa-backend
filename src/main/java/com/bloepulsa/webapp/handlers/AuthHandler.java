package com.bloepulsa.webapp.handlers;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.bloepulsa.webapp.dto.BaseResponseDto;
import com.bloepulsa.webapp.dto.LoginRequestDto;
import com.bloepulsa.webapp.dto.LoginResponseDto;

@Path("/")
public class AuthHandler {
	
	@Path("/login")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public BaseResponseDto<LoginResponseDto> login(LoginRequestDto request) {
		BaseResponseDto<LoginResponseDto> response = new BaseResponseDto<LoginResponseDto>(0, null);
		LoginResponseDto loginResponse = new LoginResponseDto("some random token");
		response.setData(loginResponse);
		response.setMessage("success");
		
		return response;
		
	}

}
