package com.bloepulsa.webapp.components;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class MysqlConnector {
	private static Connection connection;
	
	public MysqlConnector(String uri, String username, String password) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(uri, username, password);
		}catch(ClassNotFoundException | SQLException ex) {
			ex.printStackTrace();
		}
	}

	public static Connection getConnection() {
		return connection;
	}
}
