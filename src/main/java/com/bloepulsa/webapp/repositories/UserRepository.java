package com.bloepulsa.webapp.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bloepulsa.webapp.entities.User;
import com.bloepulsa.webapp.components.*;

public class UserRepository implements IUserRepository {

	private Connection db = MysqlConnector.getConnection();
	private PreparedStatement preparedStatement;
	private ResultSet resultSet;

	@Override
	public List<User> getAll(int limit, int page) throws SQLException {
		preparedStatement = db.prepareStatement("SELECT * FROM users LIMIT ? OFFSET ?");
		preparedStatement.setInt(1, limit);
		preparedStatement.setInt(2, page);

		resultSet = preparedStatement.executeQuery();

		List<User> users = new ArrayList<User>();
		while (resultSet.next()) {
			users.add(new User(resultSet.getLong("id"), resultSet.getString("username"),
					resultSet.getString("password"), 0));
		}

		preparedStatement.close();
		resultSet.close();

		return users.size() > 0 ? users : null;

	}

	@Override
	public User getById(long id) throws SQLException {
		preparedStatement = db.prepareStatement("SELECT * from users WHERE id = ?");
		preparedStatement.setLong(1, id);

		resultSet = preparedStatement.executeQuery();

		if (resultSet.isFirst()) {
			return new User(resultSet.getLong("id"), resultSet.getString("username"), resultSet.getString("password"),
					0);
		}
		return null;
	}

	@Override
	public User getByUserEmail(String email) throws SQLException {
		preparedStatement = db.prepareStatement("SELECT * from users WHERE email = ?");
		preparedStatement.setString(1, email);

		resultSet = preparedStatement.executeQuery();

		if (resultSet.isFirst()) {
			return new User(resultSet.getLong("id"), resultSet.getString("username"), resultSet.getString("password"),
					0);
		}
		return null;
	}

}
