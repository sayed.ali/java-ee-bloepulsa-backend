package com.bloepulsa.webapp.repositories;

import java.sql.SQLException;
import java.util.List;

import com.bloepulsa.webapp.entities.User;

public interface IUserRepository {
	public List<User> getAll(int limit, int page) throws SQLException;
	
	public User getById(long id) throws SQLException;
	
	public User getByUserEmail(String email) throws SQLException;

}
