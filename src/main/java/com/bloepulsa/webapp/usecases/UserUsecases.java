package com.bloepulsa.webapp.usecases;

import java.sql.SQLException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.bloepulsa.webapp.entities.User;
import com.bloepulsa.webapp.repositories.IUserRepository;

/**
 * Session Bean implementation class UserUsecases
 */
@Stateless
@LocalBean
public class UserUsecases {

    /**
     * Default constructor. 
     */
	
	@Inject
	private IUserRepository userRepository;
	
    public UserUsecases() {
        // TODO Auto-generated constructor stub
    }
    
    public String login(String username, String password) {
    	
    	User user = null;
		try {
			user = this.userRepository.getByUserEmail(username);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	if (user.getPassword().equals(password)) {
    		return "token";
    	}
    	
    	return null;
    	
    }

}
